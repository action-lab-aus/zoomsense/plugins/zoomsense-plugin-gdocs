import * as admin from "firebase-admin";
import { ZoomSenseServerFirebaseDB } from "@zoomsense/zoomsense-firebase";

export const db = new ZoomSenseServerFirebaseDB(admin.database());