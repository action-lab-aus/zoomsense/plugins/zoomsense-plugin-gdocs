import * as admin from "firebase-admin";
admin.initializeApp();

export { googleAuth, googleAuthRedirect } from "./googleAuth";
export {
  googleDocsAddOrUpdateDocument,
  googleDocsDeleteDocument,
  googleDocsDistributeAndStartWatchingCopies,
  googleDocsRedistributeToLateJoiners,
  googleDocsWatchingWebHook
} from "./googleDocs";