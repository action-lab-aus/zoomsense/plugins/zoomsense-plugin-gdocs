import { docs_v1 } from "googleapis";

/**
 * Wiping a doc's content requires knowing the start and end index of the content.
 */
export async function getWipeDocumentContentsRequest(gdocsApi: docs_v1.Docs, documentId: string) {
  // 1) get content 2) get end index 3) delete using deleteContentRange
  const documentResponse = await gdocsApi.documents.get({ documentId });
  const content = documentResponse.data.body?.content;
  if (content?.length) {
    // not allowed to delete the final newline character.
    const endIndex = content[content.length - 2].endIndex;
    return {
      deleteContentRange: {
        range: {
          startIndex: 1,
          endIndex: endIndex,
        },
      },
    };
  }
  return null;
}

export function getInsertSectionResponseAreaRequest(sectionIndex: number) {
  return [
    {
      insertTable: {
        rows: 1,
        columns: 1,
        location: {
          index: 1,
        },
      },
    },
    {
      updateTableRowStyle: {
        tableStartLocation: { index: 2 },
        rowIndices: [],
        tableRowStyle: {
          minRowHeight: {
            magnitude: 250,
            unit: "PT",
          },
        },
        fields: "*",
      },
    },
    {
      createNamedRange: {
        name: `zoomsense_section_${sectionIndex}`,
        range: {
          startIndex: 4,
          endIndex: 6,
        },
      },
    },
    {
      insertText: {
        location: {
          index: 1,
        },
        text: "Enter your response into the box below:",
      },
    },
  ]
}

export function getInsertSectionContentRequest(content: string, title: string) {
  return [
    {
      insertText: {
        location: {
          index: 1,
        },
        text: "\n" + content + "\n",
      },
    },
    {
      updateParagraphStyle: {
        paragraphStyle: {
          namedStyleType: "NORMAL_TEXT",
        },
        range: {
          startIndex: 1,
          endIndex: content.length + 2,
        },
        fields: "namedStyleType",
      },
    },
    {
      insertText: {
        location: {
          index: 1,
        },
        text: title,
      },
    },
    {
      updateParagraphStyle: {
        paragraphStyle: {
          namedStyleType: "HEADING_2",
          spaceAbove: {
            magnitude: 10.0,
            unit: "PT",
          },
          spaceBelow: {
            magnitude: 10.0,
            unit: "PT",
          },
        },
        range: {
          startIndex: 2,
          endIndex: 2,
        },
        fields: "namedStyleType,spaceAbove,spaceBelow",
      },
    },
  ];
}

export function getInsertPageBreakRequest() {
  return [
    {
      insertSectionBreak: {
        sectionType: "NEXT_PAGE",
        location: {
          index: 1,
        },
      },
    },
    {
      insertText: {
        location: {
          index: 1,
        },
        text: "\n",
      },
    },
    {
      updateParagraphStyle: {
        paragraphStyle: {
          namedStyleType: "NORMAL_TEXT",
        },
        range: {
          startIndex: 1,
          endIndex: 1,
        },
        fields: "namedStyleType",
      },
    },
  ];
}