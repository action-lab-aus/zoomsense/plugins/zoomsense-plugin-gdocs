import * as functions from "firebase-functions";
import * as crypto from "crypto";
import { docs_v1, drive_v3, google } from "googleapis";
import * as moment from "moment-timezone";
import * as retry from "async-retry";
import { typedFirebaseFunctionRef, TypedFirebasePath, ZoomSenseMeeting } from "@zoomsense/zoomsense-firebase";

import {
  getGDocsDriveFolderIdFirebasePath,
  GDocsGoogleDocument,
  getGDocsTokenAccountRootDriveFolderIdFirebasePath,
  getGDocsBreakoutRoomDriveFolderIdFirebasePath,
  getGDocsDocumentFirebasePath,
  getGDocsLinkedAccountFirebasePath,
  getGDocsBreakoutRoomFirebasePath,
  getGDocsBreakoutRoomDocumentFirebasePath,
  getGDocsBreakoutRoomDocumentRevisionFirebasePath
} from "../src-shared";
import { oauth2Client } from "./googleAuth";
import {
  getInsertPageBreakRequest,
  getInsertSectionContentRequest,
  getInsertSectionResponseAreaRequest,
  getWipeDocumentContentsRequest
} from "./googleDocEdits";
import { db } from "./db";

const environment = process.env.GCLOUD_PROJECT ? functions.config()[process.env.GCLOUD_PROJECT] : {};

function requiredDataParams<T extends object, K extends keyof T>(data: any, ...paramNames: K[]): T {
  const result = {} as T;
  for (const fieldName of paramNames) {
    const field = data[fieldName];
    if (!field || (typeof(field) === "string" && !field.trim())) {
      throw new functions.https.HttpsError("invalid-argument", `Required field "${String(fieldName)}" missing from data.`);
    }
    result[fieldName] = field;
  }
  return result;
}

export const googleDocsDeleteDocument = functions.https.onCall(async (data, context): Promise<void> => {
  if (!context.auth?.token) {
    throw new functions.https.HttpsError("unauthenticated", "unauthenticated");
  }
  const userId = context.auth.uid;

  const { meetingId, docId }: {meetingId: string, docId: string} = requiredDataParams(data, "meetingId", "docId");

  const documentSnapshot = await db.ref(getGDocsDocumentFirebasePath(meetingId, docId)).get();

  if (!documentSnapshot.exists()) {
    throw new functions.https.HttpsError("invalid-argument", `Document ${docId} not found for meeting ${meetingId}`);
  }
  const document = documentSnapshot.val()!;

  const googleAccountSnapshot = await db.ref(getGDocsLinkedAccountFirebasePath(userId, document.googleAccountId)).get();

  if (!googleAccountSnapshot.exists()) {
    throw new functions.https.HttpsError("permission-denied", "Google account not found");
  }
  const googleAccount = googleAccountSnapshot.val()!;

  oauth2Client.setCredentials({
    refresh_token: googleAccount.token,
  });
  const driveApi = google.drive({ version: "v3", auth: oauth2Client });

  await driveApi.files.delete({ fileId: docId });

  await db.ref(getGDocsDocumentFirebasePath(meetingId, docId)).remove();
});

async function getOrCreateFolder(driveApi: drive_v3.Drive, firebasePath: TypedFirebasePath<string, string>, foldername: string, parent?: string): Promise<string | undefined> {
  try {
    // Check if we have a folder ID already stored at the given Firebase path.
    const folderIdSnapshot = await db.ref(firebasePath).once("value");

    const fileId = folderIdSnapshot.val();
    if (fileId) {
      // Check that it still exists.
      if (await driveApi.files.get({ fileId })) {
        return fileId;
      }
    }

    // No valid folder exists on the user's drive - create it and store the new ID
    const newFolder = await driveApi.files.create({
      requestBody: {
        name: foldername,
        mimeType: "application/vnd.google-apps.folder",
        parents: parent ? [parent] : undefined
      },
      fields: "id"
    });

    await db.ref(firebasePath).set(newFolder.data.id!);

    return newFolder.data.id ?? undefined;
  } catch (error) {
    functions.logger.error(error);
    return undefined;
  }
}

async function getOrCreateRootDocumentsFolder(driveApi: drive_v3.Drive, userId: string, googleId: string) {
  return await getOrCreateFolder(
    driveApi,
    getGDocsTokenAccountRootDriveFolderIdFirebasePath(userId, googleId),
    "ZoomSense Documents"
  );
}

function getMeetingDocFolderName(meeting: ZoomSenseMeeting): string {
  return `${meeting.topic} - ${moment(meeting.startTime).format("Do MMM YYYY, HH:mm")}`;
}

export const googleDocsAddOrUpdateDocument = functions.https.onCall(async (data, context) => {
  if (!context.auth?.token) {
    throw new functions.https.HttpsError("unauthenticated", "unauthenticated");
  }
  const userId = context.auth.uid;

  const { meetingId, doc }: { meetingId: string, doc: GDocsGoogleDocument } = requiredDataParams(data, "meetingId", "doc");

  if (!doc.sections) doc.sections = [];

  doc.distributed = false;

  let documentId = data.documentId as (string | undefined | null); // optional, passed if editing existing doc
  const creatingNewDoc = !documentId;

  const meetingSnapshot = await db.ref(`meetings/${userId}/${meetingId}`).once("value");
  if (!meetingSnapshot.exists()) {
    throw new functions.https.HttpsError("failed-precondition", "Meeting not found for this user");
  }
  const meeting = meetingSnapshot.val()!;

  const googleAccount = await db.ref(getGDocsLinkedAccountFirebasePath(userId, doc.googleAccountId)).get();
  if (!googleAccount.exists()) {
    throw new functions.https.HttpsError("permission-denied", `Google account ${doc.googleAccountId} not found`);
  }

  oauth2Client.setCredentials({
    refresh_token: googleAccount.val()!.token,
  });
  const docsApi = google.docs({ version: "v1", auth: oauth2Client });
  const driveApi = google.drive({ version: "v3", auth: oauth2Client });

  const docRequests: object[] = [];

  if (documentId) {
    // Document title can't be changed through Docs API, do a separate call to Drive.
    await driveApi.files.update({
      fileId: documentId,
      requestBody: {
        name: doc.title
      }
    });
    const wipeContentsRequest = await getWipeDocumentContentsRequest(docsApi, documentId);
    if (wipeContentsRequest) {
      docRequests.push(wipeContentsRequest);
    }
  } else {
    const result = await docsApi.documents.create({ requestBody: { title: doc.title } });
    documentId = result.data.documentId!;
  }

  // Loop backwards so that we don't have to faff around with getting the right index to write to (i.e. write the
  // document from the bottom up).
  for (let sectionIndex = doc.sections.length - 1; sectionIndex >= 0; sectionIndex--) {
    const sectionData = doc.sections[sectionIndex].data;

    if (sectionData.response) {
      // Add section's response area to doc
      docRequests.push(getInsertSectionResponseAreaRequest(sectionIndex));
    }

    // Add section's content (e.g. activity instructions) to doc
    docRequests.push(getInsertSectionContentRequest(sectionData.content ?? "", sectionData.title));

    // If there's a section above this in the document put in a page break
    if (sectionIndex > 0 && doc.sections[sectionIndex - 1].data.response) {
      docRequests.push(getInsertPageBreakRequest());
    }
  }

  if (docRequests.length > 0) {
    await docsApi.documents.batchUpdate({
      documentId,
      requestBody: {
        requests: docRequests
      }
    });
  }

  if (creatingNewDoc) {
    // Google Docs API doesn't let you specify a parent folder when creating, so move it afterwards.
    // Existing documents should already be in place... if not, we don't care - we access by ID not filepath anyway.
    // Let the user move it if they want to.
    const rootFolderId = await getOrCreateRootDocumentsFolder(
      driveApi,
      userId,
      doc.googleAccountId
    );
    const folderName = getMeetingDocFolderName(meeting);
    const parentFolderId = await getOrCreateFolder(
      driveApi,
      getGDocsDriveFolderIdFirebasePath(meetingId),
      folderName,
      rootFolderId
    );

    // Get current folder IDs in order to remove them.
    const currentParents = await driveApi.files.get({
      fileId: documentId,
      fields: "parents",
    });
    const currentParentIds = currentParents.data.parents?.join(",");
    // remove current parent folders and assign the proper one
    await driveApi.files.update({
      fileId: documentId,
      addParents: parentFolderId,
      removeParents: currentParentIds,
      fields: "id",
    });
  }

  await db.ref(getGDocsDocumentFirebasePath(meetingId, documentId)).set(doc);

});

/**
 * If a user joins a breakout room late, we want to 'redistribute' any Google Docs for that breakout room to the
 * late joiner.
 */
export const googleDocsRedistributeToLateJoiners = typedFirebaseFunctionRef(functions.database.ref)(
    "data/activeSpeakers/{meetingId}/{sensorId}/current/userList/{userNumber}"
  )
  .onCreate(async (snapshot, context) => {
    const newUser = snapshot.val()!;
    const { meetingId, sensorId } = context.params;
    const isSensorInBO = (
      await db
        .ref(`data/chats/${meetingId}/${sensorId}/isInBO`)
        .once("value")
    ).val();
    const activeSpeakersNode = (
      await db
        .ref(`data/activeSpeakers/${meetingId}`)
        .once("value")
    ).val();
    // If we are in the main room, or the user role is a host, return early
    if (!activeSpeakersNode || !isSensorInBO || newUser.userRole !== "Attendee") {
      return;
    }
    // If there are no docs for this breakout room, return early.
    const breakoutRoomSnapshot = await db.ref(getGDocsBreakoutRoomFirebasePath(meetingId, sensorId)).get();
    const breakoutRoom = breakoutRoomSnapshot.val();
    if (!breakoutRoom?.documents) {
      return;
    }
    const allMainRoomStartTimeStamps = Object.values(activeSpeakersNode)
      .map((sensorData) =>
        !sensorData.history ? [] : Object.keys(sensorData.history)
          .filter((timestamp) => (!sensorData.history![Number(timestamp)].isInBO))
      )
      .reduce((acc, el) => acc.concat(el), []);

    // We know that we are in a BO Room, that means that the following
    // is the start time of the most recent main room
    const startTimeOfLastMainRoom = Math.max(
      ...allMainRoomStartTimeStamps.map((val) => parseInt(val))
    );
    const messagesRef = db.ref(
      `data/chats/${meetingId}/${sensorId}/message`
    );
    for (const document of Object.values(breakoutRoom.documents)) {
      if (document.distributed !== undefined && document.distributed > startTimeOfLastMainRoom) {
        const msg = `A document has been made available to your room. Go to https://docs.google.com/document/d/${document.driveCopyId}/edit# to view and edit it.`;
        messagesRef.push({ msg, receiver: Number(snapshot.key) });
      }
    }
  });

export const googleDocsDistributeAndStartWatchingCopies = functions.https.onCall(async (data, context) => {
  if (!context.auth?.token) {
    throw new functions.https.HttpsError("unauthenticated", "unauthenticated");
  }
  const userId = context.auth.uid;

  const { documentId, meetingId, roomIds }: { documentId: string, meetingId: string, roomIds: string[] }
    = requiredDataParams(data, "documentId", "meetingId", "roomIds");

  if (roomIds.length === 0) {
    throw new functions.https.HttpsError("invalid-argument", "No room IDs passed");
  }

  const meetingHostId = data.hostId ?? userId;

  // Get the meeting details from RTDB
  const meetingSnapshot = await db
    .ref(`meetings/${meetingHostId}/${meetingId}`)
    .once("value");
  if (!meetingSnapshot.exists()) {
    throw new functions.https.HttpsError("failed-precondition", "Meeting not found");
  }
  const meeting = meetingSnapshot.val()!;

  const documentSnapshot = await db.ref(getGDocsDocumentFirebasePath(meetingId, documentId)).get();
  if (!documentSnapshot.exists()) {
    throw new functions.https.HttpsError("failed-precondition", `Document ${documentId} not found for meeting ${meetingId}`)
  }
  const document = documentSnapshot.val()!;

  const googleAccountSnapshot = await db.ref(getGDocsLinkedAccountFirebasePath(meetingHostId, document.googleAccountId)).get();
  if (!googleAccountSnapshot.exists()) {
    throw new functions.https.HttpsError("permission-denied", "Google account not found");
  }
  const googleAccount = googleAccountSnapshot.val()!;

  // Sensors need to be in breakout rooms so that we can distribute the links to the doc copies
  const chats = (await db
    .ref(`data/chats/${meetingId}`)
    .once("value")).val();

  if (!chats) {
    throw new functions.https.HttpsError("failed-precondition", "Sensors are not yet all in the breakout rooms");
  }

  oauth2Client.setCredentials({ refresh_token: googleAccount.token });
  const driveApi = google.drive({ version: "v3", auth: oauth2Client });

  // Get and store the state of the parent document at the time of copying.
  // This will be used to see if/how the groups' copies have been edited.
  const parentState = await getResponses(document.sections?.length ?? 0, documentId);
  await db.ref(getGDocsDocumentFirebasePath(meetingId, documentId)).update({initialState: parentState});

  // prep parent Google Drive folders
  const rootFolderId = await getOrCreateRootDocumentsFolder(
    driveApi,
    userId,
    document.googleAccountId
  );
  const meetingFolderName = getMeetingDocFolderName(meeting);
  const meetingFolderId = await getOrCreateFolder(
    driveApi,
    getGDocsDriveFolderIdFirebasePath(meetingId),
    meetingFolderName,
    rootFolderId
  );

  // calculate how long the files should be watched for
  const meetingRemaining = moment.duration(
    moment(meeting.endTime).diff(moment(new Date()))
  );
  const watchDuration = Math.max(meetingRemaining.asMinutes() + 30, 60); // minimum 60 mins

  await Promise.all(
    roomIds
      .filter((roomId) => (chats[roomId].isInBO))
      .map((roomId) => (copyAndWatchFile(
        driveApi,
        meetingId,
        roomId,
        meetingFolderId,
        documentId,
        document.title,
        meetingHostId,
        watchDuration
      )))
  );

  await db.ref(getGDocsDocumentFirebasePath(meetingId, documentId)).update({distributed: true});

});

// Create sub-folders, copy doc into those folders and then start watching for changes
async function copyAndWatchFile(
  driveApi: drive_v3.Drive,
  meetingId: string,
  roomId: string,
  meetingFolderId: string | undefined,
  documentId: string,
  docTitle: string,
  userId: string,
  watchDuration: number
) {
  const subFolderId = await getOrCreateFolder(
    driveApi,
    getGDocsBreakoutRoomDriveFolderIdFirebasePath(meetingId, roomId),
    roomId,
    meetingFolderId
  );

  const copyResult = await driveApi.files.copy({
    fileId: documentId,
    fields: "id",
    requestBody: {
      parents: subFolderId ? [subFolderId] : undefined,
    },
  });

  if (!copyResult.data.id) {
    throw new functions.https.HttpsError("internal", `Failed to copy document ${documentId} to subfolder ${roomId}`);
  }

  await driveApi.permissions.create({
    fileId: copyResult.data.id,
    fields: "id",
    requestBody: {
      type: "anyone",
      role: "writer",
    },
  });

  await db.ref(getGDocsBreakoutRoomDocumentFirebasePath(meetingId, roomId, documentId)).update({
    driveCopyId: copyResult.data.id,
    distributed: Date.now()
  });

  const msg = `A new document, '${docTitle}', has been made available to your room. Go to https://docs.google.com/document/d/${copyResult.data.id}/edit# to view and edit it.`;

  await db
    .ref(`/data/chats/${meetingId}/${roomId}/message`)
    .push({ msg, receiver: 0 });

  await startWatchingFile(
    documentId,
    copyResult.data.id,
    meetingId,
    roomId,
    userId,
    watchDuration,
    driveApi
  );
}

async function startWatchingFile(
  originalDocId: string,
  copyId: string,
  meetingId: string,
  roomId: string,
  userId: string,
  watchMinutes: number,
  driveApi: drive_v3.Drive
) {
  const token = JSON.stringify({
    userId,
    meetingId,
    copyId,
    originalDocId,
    roomId,
  });

  // Google API can seemingly return error codes even if our code is right, so set up retries.
  // TODO implement this across all API calls
  await retry(
    async (bail) => {
      const response = await driveApi.files.watch({
        fileId: copyId,
        requestBody: {
          kind: "api#channel",
          id: crypto.randomBytes(16).toString("hex"),
          resourceId: copyId,
          token, // arbitrary string that gets returned to web hook
          expiration: moment().add(watchMinutes, "minutes").valueOf().toString(),
          type: "web_hook",
          address: `${environment.functions_url}googleDocsWatchingWebHook/`, // this won't work for localhost
          payload: true,
        },
      });
      if (response === null) {
        bail(new functions.https.HttpsError("not-found", "Not found"));
        return;
      } else if (response.status !== 200) {
        throw new functions.https.HttpsError("internal", `Returned ${JSON.stringify(response)}`);
      }
      return response;
    },
    {
      retries: 5,
      onRetry: (err) => {
        functions.logger.info("retrying:", err);
      },
    }
  );
}

// Handle text and inline content.
function readParagraphElement(document: docs_v1.Schema$Document, element: docs_v1.Schema$ParagraphElement): string {
  if (element.textRun) {
    // standard text
    return element.textRun.content ?? "";
  }
  if (element.inlineObjectElement) {
    const objId = element.inlineObjectElement.inlineObjectId;
    let imgTag = "\n[img]404[/img]";

    try {
      const embeddedObj = !objId ? undefined : document.inlineObjects?.[objId].inlineObjectProperties?.embeddedObject;
      if (embeddedObj?.imageProperties) {
        // this is an image
        imgTag = `[img]${embeddedObj.imageProperties.contentUri}[/img]`;
      } else if (embeddedObj?.embeddedDrawingProperties) {
        // this is a shape/drawing
        // can't find any way to meaningfully reference them externally,
        // so storing the ID in case we can do it later
        imgTag = `[drawing]${objId}[/drawing]`;
      }
    } catch (exception) {
      functions.logger.info(exception);
    }

    return imgTag;
  }

  return "[unknown]";
}

// https://developers.google.com/docs/api/samples/extract-text
function readStructuralElementsRecursively(document: docs_v1.Schema$Document, elements?: docs_v1.Schema$StructuralElement[]) {
  const result: string[] = [];
  if (elements) {
    for (const element of elements) {
      if (element.paragraph?.elements) {
        result.push(...element.paragraph.elements.map((elem) => (readParagraphElement(document, elem))))
      } else if (element.table?.tableRows) {
        // The text in table cells are in nested Structural Elements, so this is recursive
        result.push("[table]");
        result.push(...element.table.tableRows.map((row) => (
          `[row]${
            row.tableCells?.map((cell) => (
              readStructuralElementsRecursively(document, cell.content)
            )).join("") ?? ""
          }[/row]`
        )))
        result.push("[/table]");
      }
    }
  }
  return result.join("");
}

async function getResponses(numSections: number, documentId: string): Promise<string[] | undefined> {
  try {
    const docsApi = google.docs({ version: "v1", auth: oauth2Client });

    const document = await docsApi.documents.get({ documentId: documentId });

    const ranges = document.data.namedRanges;
    const docContent = document.data.body?.content;

    const toStore = [];

    if (docContent) {
      for (let sectionIndex = 0; sectionIndex < numSections; sectionIndex++) {
        const range = ranges?.[`zoomsense_section_${sectionIndex}`]?.namedRanges?.[0]?.ranges?.[0];
        if (range && typeof(range.startIndex) === "number" && typeof(range.endIndex) === "number") {
          // loop through document contents until we hit the right index
          for (const docSection of docContent) {
            if (typeof(docSection.startIndex) === "number"
              && typeof(docSection.endIndex) === "number"
              && docSection.startIndex <= range.startIndex
              && docSection.endIndex >= range.endIndex
            ) {
              // we know that the ranges are inside single table cells
              const sectionContents = docSection.table?.tableRows?.[0]?.tableCells?.[0]?.content;
              if (sectionContents) {
                toStore.push(
                  readStructuralElementsRecursively(document, sectionContents)
                );
              }
            }
          }
        }
      }
    }

    return toStore;
  } catch (err) {
    functions.logger.error(err);
    return undefined;
  }
}

// https://developers.google.com/drive/api/v3/push
export const googleDocsWatchingWebHook = functions.https.onRequest(
  async (req, res): Promise<void> => {
    if (req.method === "GET") {
      // Google will occasionally send GET requests here to make sure it's still authenticated and that we own the
      // domain. Need to return a "site verification code" in the response HTML.
      res.status(200).send(`
<!DOCTYPE html> <html lang="en"> <head>
  <title>Domain Verification</title>
  <meta name="google-site-verification" content="${environment.domain_verification}" />> 
</head> <body> </body> </html>`);
      return;
    }

    // If it's a POST request, it should be from Google about a Drive update.
    if (req.headers["x-goog-resource-state"] === "sync") {
      // channel has just been set up
      res.status(200).send();
      return;
    }

    const { userId, meetingId, copyId, originalDocId, roomId } = JSON.parse(String(req.headers["x-goog-channel-token"]));

    const documentSnapshot = await db.ref(getGDocsDocumentFirebasePath(meetingId, originalDocId)).get();
    if (!documentSnapshot.exists()) {
      throw new functions.https.HttpsError("not-found", "Cannot find document with those details");
    }
    const document = documentSnapshot.val()!;

    const numSections = document.sections?.length ?? 0;

    // Use the document to find the Google account that it was created with
    const googleAccount = (await db.ref(getGDocsLinkedAccountFirebasePath(userId, document.googleAccountId)).get()).val();
    // Use that Google account's auth token to start watching for changes to the doc.
    const refreshToken = googleAccount?.token;

    const breakoutRoomDocument = (await db.ref(getGDocsBreakoutRoomDocumentFirebasePath(meetingId, roomId, originalDocId)).get()).val();

    oauth2Client.setCredentials({ refresh_token: refreshToken });
    const revisionsQuery = await google
      .drive({ version: "v3", auth: oauth2Client })
      .revisions.list({
        fileId: copyId,
        pageSize: 5,
        fields: "nextPageToken, revisions/id, revisions/modifiedTime, revisions/lastModifyingUser/displayName, " +
          "revisions/lastModifyingUser/emailAddress",
        pageToken: breakoutRoomDocument?.nextPageToken
      });

    const revisions = revisionsQuery?.data?.revisions;
    if (revisions) {
      await Promise.all(revisions.map((revision) => (
        db.ref(getGDocsBreakoutRoomDocumentRevisionFirebasePath(meetingId, roomId, originalDocId, revision.id!)).update({
          modifiedAt: revision.modifiedTime!,
          modifiedBy: revision.lastModifyingUser as {displayName: string, emailAddress: string},
        })
      )));

      if (revisionsQuery.data.nextPageToken) {
        await db.ref(getGDocsBreakoutRoomDocumentFirebasePath(meetingId, roomId, originalDocId)).update({
          nextPageToken: revisionsQuery.data.nextPageToken
        });
      }

      const lastRevisionId = revisions[revisions.length - 1].id!;

      await db.ref(getGDocsBreakoutRoomDocumentRevisionFirebasePath(meetingId, roomId, originalDocId, lastRevisionId)).update({
        responses: await getResponses(numSections, copyId)
      });

    } else {
      functions.logger.debug("no revisions returned", revisionsQuery);
    }

    // Google needs webhook to return a confirmation
    res.status(200).send();
  }
);
