import * as functions from "firebase-functions";
import { getGDocsLinkedAccountFirebasePath } from "../src-shared";
import { google } from "googleapis";

import { db } from "./db";

const environment = process.env.GCLOUD_PROJECT ? functions.config()[process.env.GCLOUD_PROJECT] : {};

const localHost = (process.env.FIREBASE_AUTH_EMULATOR_HOST === "localhost:9099");
const OAUTH_REDIRECT_URI = (localHost ? environment.localhost_functions_url : environment.functions_url)
  + "GDocs-googleAuthRedirect";

export const oauth2Client = new google.auth.OAuth2(
  environment.gapi.client_id,
  environment.gapi.client_secret,
  OAUTH_REDIRECT_URI
);

// OAuth docs: https://github.com/googleapis/google-api-nodejs-client#oauth2-client

// Starts OAuth sign-in process
// User should be logged into an existing zoomsense account
export const googleAuth = functions.https.onCall(async (data, context) => {
  if (!context.auth?.token) {
    throw new functions.https.HttpsError("unauthenticated", "unauthenticated");
  }

  const returnUrl = decodeURIComponent(data.returnUrl) || environment.site_url;
  const state = {
    uid: context.auth.uid,
    returnUrl
  };

  return oauth2Client.generateAuthUrl({
    // offline gets refresh_token
    access_type: "offline",
    scope: [
      "https://www.googleapis.com/auth/userinfo.email",
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/drive.file",
    ],
    state: JSON.stringify(state),
    redirect_uri: OAUTH_REDIRECT_URI,
    // prompt: "consent", // This will require the user to give consent each time, which can be useful when developing/debugging
    prompt: "select_account", // This will always prompt the user to choose which Google account they want to use.
  });
});

export const googleAuthRedirect = functions.https.onRequest(async (req, res) => {
  let returnUrl = environment.siteUrl;
  try {
    if (!req.query.state || !req.query.code) {
      functions.logger.error("No state or code query parameters set on request");
      res.redirect(environment.site_url);
      return;
    }
    const state = JSON.parse(String(req.query.state));
    returnUrl = state.returnUrl;

    const { tokens } = await oauth2Client.getToken(String(req.query.code));

    if (!tokens.refresh_token) {
      functions.logger.error("No refresh token");
      res.redirect(returnUrl);
      return;
    }

    oauth2Client.setCredentials(tokens);

    const { data: profile } = await google.people("v1").people.get({
      resourceName: "people/me",
      personFields: "emailAddresses,names,photos",
      key: environment.gapi.api_key,
      auth: oauth2Client,
    });

    const googleId = profile.emailAddresses![0].metadata!.source!.id!;

    // Store account and token details
    await db.ref(getGDocsLinkedAccountFirebasePath(state.uid, googleId)).update({
      email: profile.emailAddresses![0].value!,
      picture: profile.photos![0].url!,
      name: profile.names![0].displayName!,
      token: tokens.refresh_token,
    });
    functions.logger.info("Redirecting to", returnUrl);

    res.redirect(returnUrl);
  } catch (err) {
    functions.logger.error(err);
    res.redirect(returnUrl);
  }
});