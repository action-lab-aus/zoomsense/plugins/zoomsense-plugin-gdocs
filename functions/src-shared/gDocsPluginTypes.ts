export interface GDocsPluginGoogleDocumentSectionData {
  id: number;
  title: string;
  content: string;
  response?: boolean;
}

export interface GDocsPluginGoogleDocumentSection {
  type: any;
  ref: number;
  data: GDocsPluginGoogleDocumentSectionData;
}

export interface GDocsGoogleDocument {
  title: string;
  distributed: boolean;
  googleAccountId: string;
  sections?: GDocsPluginGoogleDocumentSection[];
  initialState?: string[];
}

export interface GDocsBreakoutRoomDocumentRevision {
  modifiedAt: string;
  modifiedBy: {
    displayName: string;
    emailAddress: string;
  };
  responses?: string[];
}

export interface GDocsBreakoutRoomDocument {
  driveCopyId: string;
  distributed?: number;
  nextPageToken?: string;
  revisions?: {[revisionId: string]: GDocsBreakoutRoomDocumentRevision};
}

export interface GDocsBreakoutRoom {
  driveFolderId: string;
  documents?: {[documentId: string]: GDocsBreakoutRoomDocument};
}

export interface GDocsPluginDataMeeting {
  documents?: {[documentId: string]: GDocsGoogleDocument};
  rooms?: {[sensorId: string]: GDocsBreakoutRoom};
  driveFolderId?: string;
}

export interface GDocsPluginGoogleAccount {
  email: string;
  name: string;
  picture: string;
  token: string;
  rootDriveFolderId?: string;
}

export interface GDocsUser {
  linkedAccounts?: {[accountId: string]: GDocsPluginGoogleAccount};
}

export interface GDocsPluginRootType {
  meetings?: {[meetingId: string]: GDocsPluginDataMeeting};
  users?: {[userId: string]: GDocsUser};
}