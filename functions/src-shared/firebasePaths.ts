import { appendFirebasePath, buildFirebasePath } from "@zoomsense/zoomsense-firebase";

import { GDocsPluginRootType } from "./gDocsPluginTypes";

declare module "@zoomsense/zoomsense-firebase" {
  export interface ZoomSenseDataPlugins {
    gdocs: GDocsPluginRootType;
  }
}

const gDocsPluginBasePath = buildFirebasePath("data/plugins/gdocs");

export function getGDocsMeetingFirebasePath(meetingId: string) {
  return appendFirebasePath(gDocsPluginBasePath, `meetings/${meetingId}`);
}

// Was `meetings/${userId}/${meetingId}/gdocs/documents/${documentId}`
export function getGDocsDocumentFirebasePath(meetingId: string, documentId: string) {
  return appendFirebasePath(getGDocsMeetingFirebasePath(meetingId), `documents/${documentId}`);
}

// Was `meetings/${userId}/${meetingId}/gdocs/driveFolderId`
export function getGDocsDriveFolderIdFirebasePath(meetingId: string) {
  return appendFirebasePath(getGDocsMeetingFirebasePath(meetingId), "driveFolderId");
}

// Was `gdocsTokens/${userId}`
export function getGDocsUserFirebasePath(userId: string) {
  return appendFirebasePath(gDocsPluginBasePath, `users/${userId}`);
}

export function getGDocsUserLinkedAccountsFirebasePath(userId: string) {
  return appendFirebasePath(getGDocsUserFirebasePath(userId), "linkedAccounts");
}

// Was `gdocsTokens/${userId}/${googleAccountId}`
export function getGDocsLinkedAccountFirebasePath(userId: string, googleAccountId: string) {
  return appendFirebasePath(getGDocsUserFirebasePath(userId), `linkedAccounts/${googleAccountId}`);
}

// Was `gdocsTokens/${userId}/${googleAccountId}/driveFolderId`
export function getGDocsTokenAccountRootDriveFolderIdFirebasePath(userId: string, googleAccountId: string) {
  return appendFirebasePath(getGDocsLinkedAccountFirebasePath(userId, googleAccountId), "rootDriveFolderId");
}

// Was `data/gdocs/${meetingId}/${sensorId}`
export function getGDocsBreakoutRoomFirebasePath(meetingId: string, sensorId: string) {
  return appendFirebasePath(getGDocsMeetingFirebasePath(meetingId), `rooms/${sensorId}`);
}

// Was `data/gdocs/${meetingId}/${sensorId}/driveFolderId`
export function getGDocsBreakoutRoomDriveFolderIdFirebasePath(meetingId: string, sensorId: string) {
  return appendFirebasePath(getGDocsBreakoutRoomFirebasePath(meetingId, sensorId), "driveFolderId");
}

// Was `data/gdocs/${meetingId}/${sensorId}/${documentId}`
export function getGDocsBreakoutRoomDocumentFirebasePath(meetingId: string, sensorId: string, documentId: string) {
  return appendFirebasePath(getGDocsBreakoutRoomFirebasePath(meetingId, sensorId), `documents/${documentId}`);
}

// Was `data/gdocs/${meetingId}/${sensorId}/${documentId}/revisions/${revisionId}`
export function getGDocsBreakoutRoomDocumentRevisionFirebasePath(meetingId: string, sensorId: string, documentId: string, revisionId: string) {
  return appendFirebasePath(getGDocsBreakoutRoomDocumentFirebasePath(meetingId, sensorId, documentId), `revisions/${revisionId}`);
}